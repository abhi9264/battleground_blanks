package com.mitra;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.Normalizer;
import java.util.Scanner;
import java.util.regex.Pattern;

import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;
import com.mitra.competetion.bo.Team;
import com.mitra.competetion.bo.Teams;
import com.mitra.players.bo.Player;
import com.mitra.players.bo.Players;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class FootballClient {

	public static void main(String[] args) {
		
		Scanner scanner = new Scanner(System.in);
		String league_ids = scanner.nextLine();
		scanner.close();
		String ids[]=league_ids.split(",");
		for(int i =0;i<ids.length;i++)
		{
			String id=ids[i];
			String fileName="C:\\BattleGround\\"+id+"_out.txt";
			FileOutputStream fos =null;
			BufferedWriter bw=null;
			try {
				File fout = new File(fileName);
				 fos = new FileOutputStream(fout);
			 
				 bw = new BufferedWriter(new OutputStreamWriter(fos));
				
				Client client = Client.create();
	
				WebResource webResource = client
				   .resource("http://api.football-data.org//v1//competitions//"+id+"//teams");
	
				ClientResponse response = webResource.accept("application/json")
		                   .get(ClientResponse.class);
	
				if (response.getStatus() != 200) {
				   throw new RuntimeException("Failed : HTTP error code : "
					+ response.getStatus());
				}
	
				String output = response.getEntity(String.class);
				
				Gson gson=new Gson();
				Teams teams=gson.fromJson(output, Teams.class);
	
	
				
				for(Team team:teams.getTeams())
				{
					String playerLink=team.get_Links().getPlayers().getHref();
					
					WebResource webResource1 = client
							   .resource(playerLink);
	
							ClientResponse response1 = webResource1.accept("application/json")
					                   .get(ClientResponse.class);
	
							if (response1.getStatus() != 200) {
							   throw new RuntimeException("Failed : HTTP error code : "
								+ response1.getStatus());
							}
	
							String output1 = response1.getEntity(String.class);
							Players players=gson.fromJson(output1, Players.class);
							
							for(Player player:players.getPlayers())
							{
								String name=deAccent(player.getName().trim().replaceAll(" ", "").toUpperCase());
								bw.write(name);
								bw.newLine();
							}
				}
			  } catch (Exception e) {
	
				e.printStackTrace();
	
			  }
			finally {
				try {
					if(bw!=null)
						bw.close();
					if(fos!=null)
						fos.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

	}
	public static String deAccent(String str) {
	    String nfdNormalizedString = Normalizer.normalize(str, Normalizer.Form.NFD); 
	    Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
	    return pattern.matcher(nfdNormalizedString).replaceAll("");
	}
}
