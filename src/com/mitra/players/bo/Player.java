
package com.mitra.players.bo;

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.apache.commons.lang.builder.ToStringBuilder;

public class Player implements Serializable
{

    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("position")
    @Expose
    private String position;
    @SerializedName("jerseyNumber")
    @Expose
    private Integer jerseyNumber;
    @SerializedName("dateOfBirth")
    @Expose
    private String dateOfBirth;
    @SerializedName("nationality")
    @Expose
    private String nationality;
    @SerializedName("contractUntil")
    @Expose
    private Object contractUntil;
    @SerializedName("marketValue")
    @Expose
    private Object marketValue;
    private final static long serialVersionUID = -4913647279289268351L;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public Integer getJerseyNumber() {
        return jerseyNumber;
    }

    public void setJerseyNumber(Integer jerseyNumber) {
        this.jerseyNumber = jerseyNumber;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public Object getContractUntil() {
        return contractUntil;
    }

    public void setContractUntil(Object contractUntil) {
        this.contractUntil = contractUntil;
    }

    public Object getMarketValue() {
        return marketValue;
    }

    public void setMarketValue(Object marketValue) {
        this.marketValue = marketValue;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this).append("name", name).append("position", position).append("jerseyNumber", jerseyNumber).append("dateOfBirth", dateOfBirth).append("nationality", nationality).append("contractUntil", contractUntil).append("marketValue", marketValue).toString();
    }

}
